import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlinVersion: String by System.getProperties()
val policy: String by System.getProperties()
val indexes: String by System.getProperties()

group = "com.compiler.server"
version = "$kotlinVersion-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val kotlinDependency: Configuration by configurations.creating {
    isTransitive = false
}
val bioKotlinDependency: Configuration by configurations.creating {
    isTransitive = true
}
val kotlinJsDependency: Configuration by configurations.creating {
    isTransitive = false
}
val libJSFolder = "$kotlinVersion-js"
val libJVMFolder = kotlinVersion
val propertyFile = "application.properties"

val copyDependencies by tasks.creating(Copy::class) {
    from(kotlinDependency)
    into(libJVMFolder)
}
val copyBioKotlinDependencies by tasks.creating(Copy::class) {
    from(bioKotlinDependency)
    into(libJVMFolder)
}

val copyJSDependencies by tasks.creating(Copy::class) {
    from(files(Callable { kotlinJsDependency.map { zipTree(it) } }))
    into(libJSFolder)
}

plugins {
    id("org.springframework.boot") version "2.3.4.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.4.0-release-329"
    kotlin("plugin.spring") version "1.4.0-release-329"
    kotlin("plugin.serialization") version "1.4.0-release-329"
}

allprojects {
    repositories {
        jcenter()
        mavenLocal()
        mavenCentral()
        maven("https://cache-redirector.jetbrains.com/kotlin.bintray.com/kotlin-plugin")
        maven("https://maven.imagej.net/content/groups/public/")
        maven("https://jitpack.io")
        maven("https://dl.bintray.com/kotlin/kotlin-eap")
        maven("https://kotlin.bintray.com/kotlinx")
        maven("https://oss.sonatype.org/content/repositories/snapshots/")
        maven("https://www.ebi.ac.uk/intact/maven/nexus/content/repositories/ebi-repo")  //Uniprot JAPI
        maven("http://www.ebi.ac.uk/~maven/m2repo")  //Uniprot JAPI
    }
    afterEvaluate {
        dependencies {
            implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.10.3")
        }
    }
}


dependencies {
    kotlinDependency("junit:junit:4.12")
    kotlinDependency("org.hamcrest:hamcrest:2.2")
    kotlinDependency("com.fasterxml.jackson.core:jackson-databind:2.10.0")
    kotlinDependency("com.fasterxml.jackson.core:jackson-core:2.10.0")
    kotlinDependency("com.fasterxml.jackson.core:jackson-annotations:2.10.0")
    // Kotlin libraries
    kotlinDependency("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
    kotlinDependency("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion")
    kotlinDependency("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")
    kotlinDependency("org.jetbrains.kotlin:kotlin-test:$kotlinVersion")
    kotlinDependency("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.8")
    kotlinJsDependency("org.jetbrains.kotlin:kotlin-stdlib-js:$kotlinVersion")


    //bioKotlinDependency("de.mpicbg.scicomp:krangl:0.13")
    //bioKotlinDependency("org.nield:kotlin-statistics:1.2.1")
    //bioKotlinDependency("com.github.samtools:htsjdk:2.21.3")
    //bioKotlinDependency("org.biojava:biojava:5.3.0")
    //bioKotlinDependency("org.biojava:biojava-genome:5.3.0")
    //bioKotlinDependency("org.graalvm.sdk:graal-sdk:20.0.0")
    //bioKotlinDependency("org.apache.commons:commons-csv:1.8")
    //bioKotlinDependency("khttp:khttp:1.0.0")
    //bioKotlinDependency("com.google.guava:guava:29.0-jre")
    //bioKotlinDependency("org.ehcache:ehcache:3.8.1")

    bioKotlinDependency("org.biokotlin:biokotlin:0.02") {
        exclude(group="uk.ac.ebi.uniprot", module="japi")
    }

    //bioKotlinDependency("uk.ac.ebi.uniprot:japi:1.0.34")
    //bioKotlinDependency("org.codehaus.groovy:groovy-all:3.0.5")
    //bioKotlinDependency("javax.servlet:javax.servlet-api:4.0.1")
    //bioKotlinDependency("org.codehaus.janino:commons-compiler:2.7.8")
    //bioKotlinDependency("bsh:bsh:1.3.0")
    //bioKotlinDependency("org.apache.bsf:bsf-utils:3.1")
    //bioKotlinDependency("org.apache.bsf:bsf-engines:3.0-beta3")


    annotationProcessor("org.springframework:spring-context-indexer")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.amazonaws.serverless:aws-serverless-java-container-springboot2:1.5.1")
    implementation("junit:junit:4.12")
    implementation("org.jetbrains.intellij.deps:trove4j:1.0.20200330")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-test:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-compiler:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-script-runtime:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-js:$kotlinVersion")
    implementation("org.jetbrains.kotlin:ide-common-ij193:$kotlinVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")
    implementation("org.jetbrains.kotlin:kotlin-plugin-ij193:$kotlinVersion") {
        isTransitive = false
    }
    implementation(project(":executors", configuration = "default"))
    implementation(project(":common", configuration = "default"))

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.3.8")
}


fun buildPropertyFile() {
    rootDir.resolve("src/main/resources/${propertyFile}").apply {
        println("Generate properties into $absolutePath")
        parentFile.mkdirs()
        writeText(generateProperties())
    }
}

fun generateProperties(prefix: String = "") = """
    # this file is autogenerated by build.gradle.kts
    kotlin.version=${kotlinVersion}
    policy.file=${prefix + policy}
    indexes.file=${prefix + indexes}
    libraries.folder.jvm=${prefix + libJVMFolder}
    libraries.folder.js=${prefix + libJSFolder}
""".trimIndent()

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict", "-Xskip-metadata-version-check")
        jvmTarget = "1.8"
    }
    dependsOn(copyDependencies)
    dependsOn(copyBioKotlinDependencies)
    dependsOn(copyJSDependencies)
    dependsOn(":executors:jar")
//    dependsOn(":indexation:run")
    buildPropertyFile()
}

val buildLambda by tasks.creating(Zip::class) {
    val lambdaWorkDirectoryPath = "/var/task/"
    from(tasks.compileKotlin)
    from(tasks.processResources) {
        eachFile {
            if (name == propertyFile) { file.writeText(generateProperties(lambdaWorkDirectoryPath)) }
        }
    }
    from(policy)
//    from(indexes)
    from(libJSFolder) { into(libJSFolder) }
    from(libJVMFolder) { into(libJVMFolder) }
    into("lib") {
        from(configurations.compileClasspath) { exclude("tomcat-embed-*") }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
