# BioKotlin Compiler Server
[![Build Status](https://travis-ci.com/AlexanderPrendota/kotlin-compiler-server.svg?branch=master)](https://travis-ci.com/AlexanderPrendota/kotlin-compiler-server)
![Java CI](https://github.com/AlexanderPrendota/kotlin-compiler-server/workflows/Java%20CI/badge.svg)
[![Kotlin](https://img.shields.io/badge/Kotlin-1.4.10-orange.svg) ](https://kotlinlang.org/) 
[![GitHub license](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

A REST server for compiling and executing BioKotlin code.


## Getting Started

### Simple Spring Boot application

Download Kotlin dependencies and build an executor before starting the server:

```sh
$ ./gradlew build -x test 
```

Start the Spring Boot project. The main class: `com.compiler.server.CompilerApplication`

```sh
$ ./gradlew bootRun
```


### Get the current Kotlin version

```sh
$ curl -X GET http://localhost:8080/versions
```


The server also supports an [API](https://github.com/JetBrains/kotlin-playground) for the Kotlin Playground library. 

## Adding additional Kotlin libraries

_TBD_


## Contact
* Brandon Monier (bm646@cornell.edu)


